#include "types/itemid.hpp"

#include "types/test_itemid.hpp"

namespace pftests {
namespace types {


void Test_ItemId::demoTestCase()
{
    QVERIFY(true); // check that a condition is satisfied
    QCOMPARE(1, 1); // compare two values
}

} /* namespace types */
} /* namespace test */
