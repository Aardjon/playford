/**
 * Test suite for the ItemId class.
 */

#include <QtTest>

#ifndef TESTS_TYPES_ITEMID_HPP_
#define TESTS_TYPES_ITEMID_HPP_

namespace pftests {
namespace types {

class Test_ItemId: public QObject {
	Q_OBJECT

private slots:
	void demoTestCase();
};

} /* namespace types */
} /* namespace test */

#endif /* TESTS_TYPES_ITEMID_HPP_ */
