/*
 * test_dancefile.cpp
 *
 *  Created on: 17.01.2022
 *      Author: thomas
 */

#include "storage/dancefile/writeabledancefile.hpp"
#include "test_dancefile.hpp"

namespace pftests {
namespace storage {
namespace dancefile {


const char * TestDanceFile::TEST_DB_FILENAME = "/tmp/test.pf";


void TestDanceFile::test_dbcreation()
{
    using namespace ::storage::dancefile;
    QString filename(TEST_DB_FILENAME);

    // Create a new, empty database file
    {
        WriteableDanceFile dancefile(filename);
    }

    // Validate the created database
    QVERIFY(QFile::exists(filename) == true);
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(filename);
    QVERIFY(database.open() == true);

    // Verify schema version
    QSqlQuery query("select version from schema_version", database);
    QCOMPARE(query.next(), true);
    uint32_t version = query.value(0).toUInt();
    QCOMPARE(version, 1);
    QCOMPARE(query.next(), false);

    // Verfiy created tables
    QStringList expectedTables;
    expectedTables << "schema_version";
    expectedTables << "dances";
    expectedTables << "moves";
    expectedTables << "positions";

    QStringList foundTables = database.tables();
    QCOMPARE(foundTables.size(), expectedTables.size());
    for(auto tableNameIter=foundTables.begin(); tableNameIter!=foundTables.end(); ++tableNameIter)
    {
        QVERIFY(expectedTables.contains(*tableNameIter) == true);
    }
}


void TestDanceFile::test_createDance()
{
    using namespace ::storage::dancefile;
    QString filename(TEST_DB_FILENAME);

    // Write a new Dance entry into a newly created database
    uint32_t generatedDanceId = 0;
    {
        WriteableDanceFile dancefile(filename);
        generatedDanceId = dancefile.createDance("Testdance", "TESTFORMATION", "Testquelle");
    }

    // Validate written DB entry
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(filename);
    QVERIFY(database.open() == true);

    QSqlQuery query("select id, name, formation, source from dances", database);
    QCOMPARE(query.next(), true);
    uint32_t danceId = query.value(0).toUInt();
    QString danceName = query.value(1).toString();
    types::formation::GlobalDanceFormationId formation = query.value(2).toString().toStdString();
    QString source = query.value(3).toString();
    QCOMPARE(danceId, generatedDanceId);
    QCOMPARE(danceName, "Testdance");
    QCOMPARE(formation, "TESTFORMATION");
    QCOMPARE(source, "Testquelle");
    QCOMPARE(query.next(), false);
}


void TestDanceFile::test_addMove()
{
    using namespace ::storage::dancefile;
    QString filename(TEST_DB_FILENAME);

    // Write a new Dance entry into a newly created database
    uint32_t generatedMoveId = 0;
    uint32_t generatedDanceId = 0;
    {
        WriteableDanceFile dancefile(filename);
        generatedDanceId = dancefile.createDance("Testdance", "TETSFORMATION", "Testquelle");
        generatedMoveId = dancefile.addMove(generatedDanceId, "Fancy dance move");
    }

    // Validate written DB entry
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(filename);
    QVERIFY(database.open() == true);

    QSqlQuery query("select id, dance_id, label from moves", database);
    QCOMPARE(query.next(), true);
    uint32_t moveId = query.value(0).toUInt();
    uint32_t danceId = query.value(1).toUInt();
    QString danceName = query.value(2).toString();

    QCOMPARE(moveId, generatedMoveId);
    QCOMPARE(danceId, generatedDanceId);
    QCOMPARE(danceName, "Fancy dance move");
    QCOMPARE(query.next(), false);
}


void TestDanceFile::test_addPosition()
{
    using namespace ::storage::dancefile;
    QString filename(TEST_DB_FILENAME);

    // Write a new Dance entry into a newly created database
    uint32_t generatedMoveId = 0;
    uint32_t generatedDanceId = 0;
    {
        WriteableDanceFile dancefile(filename);
        generatedDanceId = dancefile.createDance("Testdance", "TESTFORMATION", "Testquelle");
        generatedMoveId = dancefile.addMove(generatedDanceId, "Fancy dance move");
        dancefile.addPosition(generatedMoveId, 0, 0, QPointF(13.37, 47.11), 5);
    }

    // Validate written DB entry
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(filename);
    QVERIFY(database.open() == true);

    QSqlQuery query("SELECT move_id, beat_number, person_idx, pos_x, pos_y, direction FROM positions", database);
    QCOMPARE(query.next(), true);
    uint32_t moveId = query.value(0).toUInt();
    uint32_t beatNum = query.value(1).toUInt();
    uint32_t personIdx = query.value(2).toUInt();
    qreal posX = query.value(3).toReal();
    qreal posY = query.value(4).toReal();
    uint8_t heading = query.value(5).toUInt();

    QCOMPARE(moveId, generatedMoveId);
    QCOMPARE(beatNum, 0);
    QCOMPARE(personIdx, 0);
    QCOMPARE(posX, 13.37);
    QCOMPARE(posY, 47.11);
    QCOMPARE(heading, 5);
    QCOMPARE(query.next(), false);
}


void TestDanceFile::cleanup()
{
    QFile file(TEST_DB_FILENAME);
    file.remove();
}

} /* namespace dancefile */
} /* namespace storage */
} /* namespace pftests */
