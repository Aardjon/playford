/**
 * test_dancefile.hpp
 */

#ifndef TESTS_STORAGE_DANCEFILE_TEST_DANCEFILE_HPP_
#define TESTS_STORAGE_DANCEFILE_TEST_DANCEFILE_HPP_

#include <QtGlobal>
#include <QtTest>

namespace pftests {
namespace storage {
namespace dancefile {

class TestDanceFile : public QObject {
	Q_OBJECT

private slots:
	void test_dbcreation();

    void test_createDance();

    void test_addMove();

    void test_addPosition();

    void cleanup();

private:
    static const char * TEST_DB_FILENAME;
};

} /* namespace dancefile */
} /* namespace storage */
} /* namespace pftests */

#endif /* TESTS_STORAGE_DANCEFILE_TEST_DANCEFILE_HPP_ */
