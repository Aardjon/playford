#include <QtTest>

#include "types/test_itemid.hpp"
#include "storage/dancefile/test_dancefile.hpp"


int main(int argc, char** argv)
{
    int status = 0;

    auto run_testcase = [&status, argc, argv](QObject* obj)
    {
        status |= QTest::qExec(obj, argc, argv);
        delete obj;
    };

    run_testcase(new pftests::types::Test_ItemId());
    run_testcase(new pftests::storage::dancefile::TestDanceFile());

    return status;
}
