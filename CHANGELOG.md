# Changelog

## Version 0.3.0 (release date: 2022-08-31)
 - Bugfix: Bring back missing application icon
 - Support loading a dance file provided via command line on startup (issue #22)
 - Support heading/viewing direction of dancers (issue #9)
 - Reading and writing dance files is no more robust (issue #21)
 - Bring back start formation for new dances

## Version 0.2.0 (release date: 2022-07-03)
 - GUI redesign (new art, single tool bar)
 - Support saving and loading dances to/from files (issue #7)
 - Create a new dance with specific settings (issue #6)
 - Deduce person count and start formation from dance settings (issue #6)
 - Show data of the current dance (issue #17)

## Version 0.1.0 (release date: 2021-12-21)
 - Adding and removing figures and beats
 - Modifying person positions per beat
 - Renaming figures
