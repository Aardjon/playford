REM Creates a binary release package for Windows.
REM This is basically a shortcut for some cmake and cpack calls.

@echo off
:: Variable QTPATH must be set in the environment (i.e. C:\Qt\Qt5.12.12)

echo *** Creating release package for Windows ***
echo Using Qt from %QTPATH%

set SCRIPT_LOCATION=%~dp0
set SRCPATH=%SCRIPT_LOCATION%\..
set BUILDPATH=%SRCPATH%\build

echo Source directory: %SRCPATH%
echo Build directory: %BUILDPATH%

set PATH=%QTPATH%\5.12.12\mingw73_64\bin;%QTPATH%/Tools/mingw730_64\bin;%PATH%
mkdir %BUILDPATH%
cd %BUILDPATH%

cmake -S %SRCPATH% -B . -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -DPLAYFORD_ENABLE_TESTING=false
cpack -C Release

copy "%BUILDPATH%\playford*.zip" "%SRCPATH%"

:: Other userful cmake commands:
::   cmake --build . --target playford
::   cmake --install . --prefix deploy
