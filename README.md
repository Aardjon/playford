# Playford

Application for analyzing baroque dance moves.

[![Build status](https://ci.appveyor.com/api/projects/status/rhhp403fqn90xbar?svg=true)](https://ci.appveyor.com/project/Aardjon/playford)


CI is hosted on AppVeyor: https://ci.appveyor.com/project/Aardjon/playford


## Licences

### Source code

The playford source itself is published under GPL v3, see LICENSE for details. This applies for almost all source files with the following exception:

The CMake module file `deployqt.cmake` was written by Nathan Osman and published unter MIT license, as documented in the file itself. It has not been modified.

## Art

The icon files are taken from the KDE Breeze icon theme which has been released under LGPL v3, see README and LICENSE files in res/svg directory.

The application icon was derived from a public domain portrait of John Playford published on Wikimedia: https://commons.wikimedia.org/wiki/File:John_Playford_by_David_Loggan.jpg.
