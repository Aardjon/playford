#ifndef TYPES_FORMATION_DANCEFORMATIONFACTORY_HPP
#define TYPES_FORMATION_DANCEFORMATIONFACTORY_HPP

#include "types/formation/danceformation.hpp"


namespace types { namespace formation {


class DanceFormationFactory
{
public:
    static const StdVector_DanceFormationPtr & getAll();

    static DanceFormationPtr get(const GlobalDanceFormationId& globalId);


    static void cleanupKnownFormations();
private:
    static StdVector_DanceFormationPtr m_KnownFormations;
};

}}

#endif // TYPES_FORMATION_DANCEFORMATIONFACTORY_HPP
