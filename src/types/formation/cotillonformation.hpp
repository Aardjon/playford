#ifndef TYPES_FORMATION_COTILLONFORMATION_HPP
#define TYPES_FORMATION_COTILLONFORMATION_HPP

#include "types/formation/danceformation.hpp"

namespace types { namespace formation {


class CotillonFormation : public DanceFormation {
    /**
     * https://www.regencydances.org/index.php?wL=614
     */
public:
    CotillonFormation(const GlobalDanceFormationId& globalId, const QString& label)
        :
          DanceFormation(globalId, label, 8)
    {}


    std::vector<QPointF> getDefaultPositions() const override
    {
        std::vector<QPointF> defaultPositions = {
            QPointF(25.0, -70.0),
            QPointF(-25.0, -70.0),
            QPointF(-25.0, 70.0),
            QPointF(25.0, 70.0),
            QPointF(-70.0, -25.0),
            QPointF(-70.0, 25.0),
            QPointF(70.0, 25.0),
            QPointF(70.0, -25.0)
        };
        return defaultPositions;
    }


    std::vector<uint8_t> getDefaultHeadings() const override
    {
        std::vector<uint8_t> defaultHeadings = {
            4, 4, 0, 0, 2, 2, 6, 6
        };
        return defaultHeadings;
    }
};


}}

#endif // TYPES_FORMATION_COTILLONFORMATION_HPP
