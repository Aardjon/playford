#include "types/formation/cotillonformation.hpp"
#include "types/formation/longwayformation.hpp"
#include "types/formation/danceformationfactory.hpp"
#include "types/formation/danceformationfactory.hpp"

namespace types { namespace formation {


StdVector_DanceFormationPtr DanceFormationFactory::m_KnownFormations = {
    DanceFormationPtr(new LongwayFormation("LONGWAY_DUPLE", "Longway for as many as will, duple minor", 4)), // https://www.regencydances.org/index.php?wL=1
    DanceFormationPtr(new LongwayFormation("LONGWAY_TRIPLE", "Longway for as many as will, triple minor", 6)), // https://www.regencydances.org/index.php?wL=1277
    DanceFormationPtr(new CotillonFormation("COTILLON", "Cotillon (Square)")), // https://www.regencydances.org/index.php?wL=614
};


const StdVector_DanceFormationPtr & DanceFormationFactory::getAll()
{
    return m_KnownFormations;
}



DanceFormationPtr DanceFormationFactory::get(const GlobalDanceFormationId& globalId)
{
    auto foundIter = std::find_if(m_KnownFormations.begin(), m_KnownFormations.end(), [globalId](const StdVector_DanceFormationPtr::value_type& df){ return df->getGlobalId() == globalId; });
    if(foundIter == m_KnownFormations.end())
    {
        // TODO: Invalid Value!
    }
    return *foundIter;
}


void DanceFormationFactory::cleanupKnownFormations()
{
    DanceFormationFactory::m_KnownFormations.clear();
}

}}
