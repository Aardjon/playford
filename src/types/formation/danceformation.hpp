#ifndef TYPES_FORMATION_DANCEFORMATION_HPP
#define TYPES_FORMATION_DANCEFORMATION_HPP

#include <memory>
#include <string>
#include <vector>
#include <QPointF>
#include <QString>

namespace types { namespace formation {


/** Global, unique identifier for formations.
 * Used to map saved data, so be careful with changes!
 */
typedef std::string GlobalDanceFormationId;


/** Base formation of a dance.
 * This defines things like the start formation and person count.
 */
class DanceFormation {

public:
    inline
    const GlobalDanceFormationId& getGlobalId() const { return m_GlobalId; }

    inline
    const QString& getLabel() const { return m_Label; }

    inline
    uint8_t getPersonCount() const { return m_PersonCount; }

    virtual
    std::vector<QPointF> getDefaultPositions() const = 0;

    virtual
    std::vector<uint8_t> getDefaultHeadings() const = 0;

protected:
   uint8_t m_PersonCount;

   inline
   DanceFormation(const GlobalDanceFormationId& globalId, const QString& label, uint8_t personCount)
       :
         m_PersonCount(personCount),
         m_GlobalId(globalId),
         m_Label(label)
   {}

private:
    GlobalDanceFormationId m_GlobalId;
    QString m_Label;
};


typedef std::shared_ptr<const DanceFormation> DanceFormationPtr;


/** Vector of DanceFormation objects. */
typedef std::vector<DanceFormationPtr> StdVector_DanceFormationPtr;



}}


#endif // TYPES_FORMATION_DANCEFORMATION_HPP
