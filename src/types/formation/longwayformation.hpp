#ifndef TYPES_FORMATION_LONGWAYFORMATION_HPP
#define TYPES_FORMATION_LONGWAYFORMATION_HPP

#include "types/formation/danceformation.hpp"

namespace types { namespace formation {


class LongwayFormation : public DanceFormation {
public:
    LongwayFormation(const GlobalDanceFormationId& globalId, const QString& label, uint8_t personCount)
        :
          DanceFormation(globalId, label, personCount)
    {}


    std::vector<QPointF> getDefaultPositions() const override
    {
        std::vector<QPointF> defaultPositions(this->m_PersonCount);
        const qreal columnSize = 60.0;
        uint32_t columnCount = this->m_PersonCount / 2;
        for(uint8_t personIdx=0; personIdx<this->m_PersonCount; ++personIdx)
        {
            uint32_t colNum = personIdx / 2;
            qreal xPos = (colNum * columnSize) - ((columnCount / 2.0) * columnSize);
            qreal yPos = 30.0;
            if(personIdx % 2 == 0)
            {
                yPos *= -1;
            }

            defaultPositions[personIdx] = QPointF(xPos, yPos);
        }
        return defaultPositions;
    }


    std::vector<uint8_t> getDefaultHeadings() const override
    {
        std::vector<uint8_t> defaultHeadings(this->m_PersonCount);
        for(uint8_t personIdx=0; personIdx<this->m_PersonCount; ++personIdx)
        {
            if(0 == personIdx % 2)
            {
                defaultHeadings[personIdx] = 4;
            }
            else
            {
                defaultHeadings[personIdx] = 0;
            }
        }
        return defaultHeadings;
    }
};


}}

#endif // TYPES_FORMATION_LONGWAYFORMATION_HPP
