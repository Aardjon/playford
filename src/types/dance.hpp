#ifndef TYPES_DANCE_HPP
#define TYPES_DANCE_HPP

#include <list>
#include <map>
#include <memory>

#include <QString>

#include "types/beat.hpp"
#include "types/moveid.hpp"
#include "types/formation/danceformation.hpp"


namespace types {


class Dance
{
public:

    inline
    Dance(const QString& name, types::formation::DanceFormationPtr formation, const QString& sourceString)
        :
          m_Name(name),
          m_FormationPtr(formation),
          m_SourceString(sourceString)
    {
    }

    static types::Dance createDefaultDance(
            const QString& danceName,
            types::formation::DanceFormationPtr danceFormation,
            const QString& sourceString);

    inline
    const types::formation::GlobalDanceFormationId& getGlobalFormationId() const { return m_FormationPtr->getGlobalId(); }

    inline
    const QString& getSourceString() const { return m_SourceString; }

    inline
    uint8_t getPersonCount() const { return m_FormationPtr->getPersonCount(); }

    inline
    const QString& getName() const { return m_Name; }

    inline
    const QString& getFormationLabel() const { return m_FormationPtr->getLabel(); }

    inline
    void setName(const QString& name) { m_Name = name; }

    inline
    uint32_t getMoveCount() const { return m_MoveLabels.size(); }

    inline
    StdVector_MoveId getMoveIds() const
    {
        StdVector_MoveId moveIds;
        moveIds.reserve(getMoveCount());
        for(auto moveIter=m_MoveLabels.begin(); moveIter!=m_MoveLabels.end(); ++moveIter)
        {
            moveIds.push_back(moveIter->first);
        }
        return moveIds;
    }


    inline
    const QString& getMoveLabel(MoveId moveId) const
    {
        return m_MoveLabels.at(moveId);
    }

    inline
    void setMoveLabel(MoveId moveId, const QString& moveLabel)
    {
        m_MoveLabels[moveId] = moveLabel;
    }

    inline
    MoveId addMove(const QString& moveLabel)
    {
        MoveId newMoveId = getNextFreeId<MoveId>(this->m_MoveLabels);
        m_MoveIdxToBeats.insert(std::make_pair(newMoveId, MoveToBeatMap::mapped_type()));
        m_MoveLabels.insert(std::make_pair(newMoveId, moveLabel));
        return newMoveId;
    }

    void removeMove(uint16_t moveId);


    inline
    uint32_t getBeatCount(MoveId moveId) const
    {
        return m_MoveIdxToBeats.at(moveId).size();
    }

    inline
    const StdVector_BeatId& getBeatIds(MoveId moveId) const
    {
        return m_MoveIdxToBeats.at(moveId);
    }

    inline
    BeatId addBeat(MoveId moveId)
    {
        BeatId beatId = createBeat();
        addBeatToMove(beatId, moveId);
        return beatId;
    }


    /**
      Not thread-safe!

     * @brief removeBeat
     * @param moveIdx
     * @param beatRow
     */
    void removeBeat(BeatId beatId);

    inline
    const Beat& getBeat(BeatId beatId) const
    {
        return this->m_Beats.at(beatId);
    }


    inline
    void setBeat(BeatId beatId, const Beat& beat)
    {
        auto beatIter = m_Beats.find(beatId);
        beatIter->second = beat;
    }


    MoveId getMoveOfBeat(BeatId beatId) const;

private:

    typedef std::map<uint32_t, std::vector<BeatId> > MoveToBeatMap;

    /** List of Beats and their (global) IDs. */
    typedef std::map<BeatId, Beat> BeatMap;

    /** List of Moves and their (global) IDs. */
    typedef std::map<types::MoveId, QString> MoveMap;


    QString m_Name;
    types::formation::DanceFormationPtr m_FormationPtr;
    QString m_SourceString;
    MoveMap m_MoveLabels;
    BeatMap m_Beats;
    MoveToBeatMap m_MoveIdxToBeats;


    inline
    BeatId createBeat()
    {
        BeatId newBeatId = this->getNextFreeId<BeatId>(this->m_Beats);
        Beat newBeat(*m_FormationPtr);
        m_Beats.insert(std::make_pair(newBeatId, newBeat));
        return newBeatId;
    }


    template<typename IdType, class ContainerType>
    IdType getNextFreeId(const ContainerType& container) const
    {
        IdType nextId = 0;
        while(container.end() != container.find(nextId))
        {
            ++nextId;
        }
        return nextId;
    }


    void addBeatToMove(BeatId beatId, MoveId moveId);
};



typedef std::unique_ptr<Dance> DancePtr;

}

#endif // TYPES_DANCE_HPP
