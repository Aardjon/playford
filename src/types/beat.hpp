#ifndef BEAT_HPP
#define BEAT_HPP

#include <vector>
#include <QPointF>

#include "types/formation/danceformation.hpp"

namespace types {


class Beat
{
public:
    Beat(const types::formation::DanceFormation& danceFormation);

    const QPointF& getPersonsPosition(uint32_t personIndex) const;
    void setPersonsPosition(uint32_t personIndex, const QPointF& position);

    /** Returns the heading ("viewing direction") of the requested dancer.
     * This is a value from 0 to 7, 0 being "north". Each increment rotates the person by 45° clockwise.
     *
     * @brief getPersonsHeading
     * @param personIndex
     * @return Heading of the dancer identified by personIndex.
     */
    uint8_t getPersonsHeading(uint32_t personIndex) const;
    void setPersonsHeading(uint32_t personIndex, uint8_t heading);


private:
    std::vector<QPointF> m_DestinationPositions;
    std::vector<uint8_t> m_PersonHeadings;
};


/** Unique ID for a beat. */
typedef uint16_t BeatId;

/** Vector fo Beat IDs. */
typedef std::vector<BeatId> StdVector_BeatId;

}

#endif // BEAT_HPP
