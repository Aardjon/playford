#ifndef ITEMID_HPP
#define ITEMID_HPP

#include <QModelIndex>

namespace types {


class ItemId
{
public:

    enum LevelType
    {
        LEVEL_UNKNOWN,
        LEVEL_MOVE,
        LEVEL_BEAT,
    };

    inline
    ItemId()
        :
          m_LevelType(LEVEL_UNKNOWN),
          m_ItemId(0)
    {}

    inline
    ItemId(const QModelIndex& modelIndex)
        :
          m_LevelType(static_cast<LevelType>((modelIndex.internalId() & LEVELTYPE_MASK) >> 16)),
          m_ItemId(modelIndex.internalId() & ITEMID_MASK)
    {}


    inline
    ItemId(LevelType level, uint16_t itemId)
        :
          m_LevelType(level),
          m_ItemId(itemId)
    {}


    inline
    quintptr toModelIndexId() const
    {
        quintptr idvalue = 0;

        idvalue |= (static_cast<quintptr>(m_LevelType) << 16);
        idvalue |= static_cast<quintptr>(m_ItemId);

        return idvalue;
    }


    inline
    uint16_t getItemId() const
    {
        return m_ItemId;
    }

    inline
    LevelType getLevelType() const
    {
        return m_LevelType;
    }


    inline
    bool isLevelBeat() const
    {
        return this->getLevelType() == LEVEL_BEAT;
    }

    inline
    bool isLevelMove() const
    {
        return this->getLevelType() == LEVEL_MOVE;
    }

private:

    static const quintptr LEVELTYPE_MASK = 0xF0000;
    static const quintptr ITEMID_MASK = 0x0FFFF;

    LevelType m_LevelType;
    uint16_t m_ItemId;
};

}

#endif // ITEMID_HPP
