#ifndef MOVEID_HPP
#define MOVEID_HPP

#include <vector>


namespace types {

typedef uint16_t MoveId;

typedef std::vector<MoveId> StdVector_MoveId;

}

#endif // MOVEID_HPP
