#ifndef TYPES_DANCEID_HPP
#define TYPES_DANCEID_HPP

#include <cstdint>


namespace types {

/**
 * Unique ID for a dance.
 */
typedef uint32_t DanceId;

}

#endif // TYPES_DANCEID_HPP
