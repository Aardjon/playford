#include "types/dance.hpp"

namespace types {


void Dance::addBeatToMove(BeatId beatId, MoveId moveId)
{
    auto searchIter = m_MoveIdxToBeats.find(moveId);
    if(searchIter == m_MoveIdxToBeats.end())
    {
        m_MoveIdxToBeats.insert(MoveToBeatMap::value_type(moveId, MoveToBeatMap::mapped_type()));
    }
    MoveToBeatMap::mapped_type& beatList = searchIter->second;
    beatList.push_back(beatId);
}



void Dance::removeBeat(BeatId beatId)
{
    MoveId moveId = this->getMoveOfBeat(beatId);
    MoveToBeatMap::mapped_type& beatIdList = this->m_MoveIdxToBeats.at(moveId);
    auto beatIdIter = std::find(beatIdList.begin(), beatIdList.end(), beatId);
    beatIdList.erase(beatIdIter);
    this->m_Beats.erase(beatId);
}


void Dance::removeMove(MoveId moveId)
{
    MoveToBeatMap::mapped_type& beatIdList = this->m_MoveIdxToBeats.at(moveId);
    const StdVector_BeatId beatIds = beatIdList;  // Copy the ID vector because we want to remove from the original while iterating
    for(auto iter=beatIds.begin(); iter!=beatIds.end(); ++iter)
    {
        this->removeBeat(*iter);
    }
    assert(0 == beatIdList.size());
    this->m_MoveIdxToBeats.erase(moveId);
    this->m_MoveLabels.erase(moveId);
}



MoveId Dance::getMoveOfBeat(BeatId beatId) const
{
    MoveId foundMoveId = std::numeric_limits<MoveId>::max();
    for(auto move2beatIter=m_MoveIdxToBeats.begin(); move2beatIter!= m_MoveIdxToBeats.end(); ++move2beatIter)
    {
        uint32_t currentMoveId = move2beatIter->first;
        const StdVector_BeatId& currentBeatIds = move2beatIter->second;
        auto foundIter = std::find(currentBeatIds.begin(), currentBeatIds.end(), beatId);
        if(foundIter != currentBeatIds.end())
        {
            foundMoveId = currentMoveId;
            break;
        }
    }
    return foundMoveId;
}


types::Dance Dance::createDefaultDance(
        const QString& danceName,
        types::formation::DanceFormationPtr danceFormation,
        const QString& sourceString)
{
    using namespace types;
    Dance newDance(danceName, danceFormation, sourceString);
    // Create a formation with one beat as start or default formation
    MoveId moveId = newDance.addMove("Startaufstellung");
    BeatId beatId = newDance.addBeat(moveId);
    Beat startBeat = newDance.getBeat(beatId);
    for(uint8_t personIdx=0; personIdx<danceFormation->getPersonCount(); ++personIdx)
    {
        startBeat.setPersonsPosition(personIdx, danceFormation->getDefaultPositions().at(personIdx));
        startBeat.setPersonsHeading(personIdx, danceFormation->getDefaultHeadings().at(personIdx));
    }
    newDance.setBeat(beatId, startBeat);
    return newDance;
}

}
