#include "types/beat.hpp"

namespace types {



Beat::Beat(const types::formation::DanceFormation& danceFormation)
    :
      m_DestinationPositions(danceFormation.getDefaultPositions()),
      m_PersonHeadings(danceFormation.getDefaultHeadings())
{
}


void Beat::setPersonsPosition(uint32_t personIndex, const QPointF& position)
{
    m_DestinationPositions[personIndex] = position;
}


const QPointF& Beat::getPersonsPosition(uint32_t personIndex) const
{
    return m_DestinationPositions[personIndex];
}


uint8_t Beat::getPersonsHeading(uint32_t personIndex) const
{
    return m_PersonHeadings[personIndex];
}


void Beat::setPersonsHeading(uint32_t personIndex, uint8_t heading)
{
    m_PersonHeadings[personIndex] = heading;
}


}
