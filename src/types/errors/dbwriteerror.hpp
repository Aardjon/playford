#ifndef TYPES_ERRORS_DBWRITEERROR_HPP
#define TYPES_ERRORS_DBWRITEERROR_HPP

#include <stdexcept>

namespace types { namespace errors {


/** Error to notify when writing into an (already opened) database failed. */
class DbWriteError : public std::runtime_error
{
public:
    DbWriteError(const std::string& errorMessage):
        std::runtime_error(errorMessage) {}
};

}}

#endif // TYPES_ERRORS_DBWRITEERROR_HPP
