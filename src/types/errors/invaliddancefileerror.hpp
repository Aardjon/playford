#ifndef TYPES_ERRORS_INVALIDDANCEFILEERROR_HPP
#define TYPES_ERRORS_INVALIDDANCEFILEERROR_HPP

#include <stdexcept>

namespace types { namespace errors {


/** Error to notify about an invalid dance file.
 * Exceptions of this type are thrown when reading a dance file fails because
 * of the file being illegal, malformed or corrupt. The message string should
 * contain more detailled information about the problem.
 */
class InvalidDanceFileError : public std::runtime_error
{
public:
    InvalidDanceFileError(const std::string& errorMessage):
        std::runtime_error(errorMessage) {}
};

}}

#endif // TYPES_ERRORS_INVALIDDANCEFILEERROR_HPP
