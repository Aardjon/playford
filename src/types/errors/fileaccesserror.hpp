#ifndef TYPES_ERRORS_FILEACCESSERROR_HPP
#define TYPES_ERRORS_FILEACCESSERROR_HPP

#include <stdexcept>

namespace types { namespace errors {


/** Error to notify when a file cannot be opened. */
class FileAccessError : public std::runtime_error
{
public:
    FileAccessError(const std::string& errorMessage):
        std::runtime_error(errorMessage) {}
};

}}

#endif // TYPES_ERRORS_FILEACCESSERROR_HPP
