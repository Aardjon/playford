#ifndef DANCEMODEL_HPP
#define DANCEMODEL_HPP

#include <QAbstractItemModel>

#include "types/beat.hpp"
#include "types/dance.hpp"


class DanceModel : public QAbstractItemModel
{
public:
    DanceModel(const types::Dance& dance);

    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    virtual QModelIndex parent(const QModelIndex &index) const override;

    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    const types::Beat& getBeat(const QModelIndex& index) const;

    void setBeat(const QModelIndex& index, const types::Beat& beat);

    bool removeRows(int row, int count, const QModelIndex &parent) override;

    bool insertRows(int row, int count, const QModelIndex &parent) override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    inline
    uint8_t getPersonCount() const
    {
        return m_Dance.getPersonCount();
    }

    inline
    const types::Dance& getDance() const
    {
    	return m_Dance;
    }

    /**
     * Invalidates all current model data!
     * @brief setDance
     * @param dance
     */
    void setDance(const types::Dance& dance);
private:

    types::Dance m_Dance;
};

#endif // DANCEMODEL_HPP
