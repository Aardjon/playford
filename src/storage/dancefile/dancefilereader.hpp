/**
 * Declaration of the DanceFileWriter class.
 */

#ifndef SRC_STORAGE_DANCEFILE_DANCEFILEREADER_HPP_
#define SRC_STORAGE_DANCEFILE_DANCEFILEREADER_HPP_

#include <QString>

#include "types/dance.hpp"


namespace storage {
namespace dancefile {


class DanceFileReader {
private:
    const QString& m_Filename;

public:
    DanceFileReader(const QString& filename);

    types::Dance readDanceFromFile();
};

}}

#endif // SRC_STORAGE_DANCEFILE_DANCEFILEREADER_HPP_
