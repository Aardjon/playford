#ifndef READONLYDANCEFILE_HPP
#define READONLYDANCEFILE_HPP

#include <QString>
#include <QSqlQuery>

#include "storage/dancefile/dancefilebase.hpp"


namespace storage {
namespace dancefile {


class ReadonlyDanceFile : public DanceFileBase
{
public:
    ReadonlyDanceFile(const QString& filename);

    void loadDance(IDanceFileReadResultDelegate& resultDelegate);
    void loadMoves(uint32_t danceDbId, IDanceFileReadResultDelegate& resultDelegate);
    void loadBeats(uint32_t moveDbId, IDanceFileReadResultDelegate& resultDelegate);
    void loadPositions(uint32_t moveDbId, uint32_t beatIdx, IDanceFileReadResultDelegate& resultDelegate);

private:
    void raiseDbError(const QSqlQuery& query) const;
};

}}

#endif // READONLYDANCEFILE_HPP
