/**
 * Implementation of the DanceFileReader class.
 */

#include <iostream>

#include "storage/dancefile/readonlydancefile.hpp"
#include "storage/dancefile/dancefilereader.hpp"
#include "types/formation/danceformationfactory.hpp"


namespace storage {
namespace dancefile {


class DanceFileReadDelegate: public IDanceFileReadResultDelegate
{
private:
    types::DancePtr m_DancePtr;
    uint32_t m_DanceDbId;
    std::map<uint32_t, types::MoveId> m_DbMoveIdToMoveIdMap;
    std::map<types::MoveId, std::map<uint32_t, types::BeatId> > m_MoveIdBeatNumToBeatIdMap;

public:
    inline
    uint32_t getDanceDbId() const { return m_DanceDbId; }

    std::vector<uint32_t> getMoveDbIds() const
    {
        std::vector<uint32_t> moveDbIdVector;
        moveDbIdVector.reserve(m_DbMoveIdToMoveIdMap.size());
        for(const auto& item : m_DbMoveIdToMoveIdMap)
        {
            moveDbIdVector.push_back(item.first);
        }
        return moveDbIdVector;
    }

    inline
    uint32_t getBeatCount(uint32_t moveDbId)
    {
        types::MoveId realMoveId = m_DbMoveIdToMoveIdMap.at(moveDbId);
        return m_DancePtr->getBeatCount(realMoveId);
    }

    inline
    types::Dance getDance() const { return *m_DancePtr; }

    void readDanceResultRow(
            uint32_t danceDbId,
            const QString& name,
            const types::formation::GlobalDanceFormationId& formationId,
            const QString& sourceString) override
    {
        m_DancePtr.reset(
                    new types::Dance(
                        name,
                        types::formation::DanceFormationFactory::get(formationId),
                        sourceString)
                    );
        m_DanceDbId = danceDbId;
    }

    void readMoveResultRow(uint32_t moveDbId, const QString& label) override
    {
        types::MoveId moveId = m_DancePtr->addMove(label);
        m_DbMoveIdToMoveIdMap.insert(std::make_pair(moveDbId, moveId));
    }

    void readBeatResultRow(uint32_t moveDbId, uint32_t beatIdx) override
    {
        types::MoveId realMoveId = m_DbMoveIdToMoveIdMap.at(moveDbId);
        types::BeatId beatId = m_DancePtr->addBeat(realMoveId); // The rows are sorted by beatIdx
        m_MoveIdBeatNumToBeatIdMap[realMoveId].insert(std::make_pair(beatIdx, beatId));
    }

    void readPositionResultRow(uint32_t moveDbId, uint32_t beatNum, uint32_t personIdx, const QPointF& position, uint8_t heading) override
    {
        types::MoveId realMoveId = m_DbMoveIdToMoveIdMap.at(moveDbId);
        types::BeatId beatId = this->m_MoveIdBeatNumToBeatIdMap.at(realMoveId).at(beatNum);
        types::Beat beat = m_DancePtr->getBeat(beatId);
        beat.setPersonsPosition(personIdx, position);
        beat.setPersonsHeading(personIdx, heading);
        m_DancePtr->setBeat(beatId, beat);
    }

};




DanceFileReader::DanceFileReader(const QString& filename)
    : m_Filename(filename)
{
}



types::Dance DanceFileReader::readDanceFromFile()
{
    ReadonlyDanceFile dbFile(m_Filename);
    DanceFileReadDelegate delegate;
    dbFile.loadDance(delegate);
    dbFile.loadMoves(delegate.getDanceDbId(), delegate);
    for(auto moveDbId: delegate.getMoveDbIds())
    {
        dbFile.loadBeats(moveDbId, delegate);
        for(uint32_t beatIdx=0; beatIdx<delegate.getBeatCount(moveDbId); ++beatIdx)
        {
            dbFile.loadPositions(moveDbId, beatIdx, delegate);
        }
    }

    return delegate.getDance();
}


}}
