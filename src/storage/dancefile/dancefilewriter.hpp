/**
 * Declaration of the DanceFileWriter class.
 */

#ifndef SRC_STORAGE_DANCEFILE_DANCEFILEWRITER_HPP_
#define SRC_STORAGE_DANCEFILE_DANCEFILEWRITER_HPP_


#include "types/danceid.hpp"
#include "types/dance.hpp"
#include "storage/dancefile/writeabledancefile.hpp"


namespace storage {
namespace dancefile {

class DanceFileWriter {
private:
	const types::Dance& m_Dance;

public:
	DanceFileWriter(const types::Dance& dance);
	virtual ~DanceFileWriter();

	void writeDanceToFile(const QString& filename);

private:
    void writeDance(WriteableDanceFile& database);
    void writeMove(WriteableDanceFile& database, types::DanceId danceId, types::MoveId moveId);
    void writeBeat(WriteableDanceFile& database, types::MoveId moveId, uint32_t beatIdx, types::BeatId beatId);

    static void truncateDbFile(const QString& filename);
};

} /* namespace dancefile */
} /* namespace storage */

#endif /* SRC_STORAGE_DANCEFILE_DANCEFILEWRITER_HPP_ */
