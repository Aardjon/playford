#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include "storage/dancefile/writeabledancefile.hpp"
#include "types/errors/dbwriteerror.hpp"


namespace storage {
namespace dancefile {


WriteableDanceFile::WriteableDanceFile(const QString& filename)
    : DanceFileBase(filename)
{
    this->createDbSchema();
}



types::DanceId WriteableDanceFile::createDance(
        const QString& name,
        const types::formation::GlobalDanceFormationId& formationId,
        const QString& sourceString)
{
    QSqlQuery query(this->getQuery());
    query.prepare("INSERT INTO dances(name, formation, source) VALUES(:name, :formation, :source)");
    query.bindValue(":name", name);
    query.bindValue(":formation", QString::fromStdString(formationId));
    query.bindValue(":source", sourceString);
    bool created = query.exec();
    if(!created)
    {
        throw types::errors::DbWriteError(query.lastError().text().toStdString());
    }
    return query.lastInsertId().toUInt();
}


types::MoveId WriteableDanceFile::addMove(uint32_t danceId, const QString& moveLabel)
{
    QSqlQuery query(this->getQuery());
    query.prepare("INSERT INTO moves(dance_id, label) VALUES(:dance_id, :label)");
    query.bindValue(":dance_id", danceId);
    query.bindValue(":label", moveLabel);
    bool inserted = query.exec();
    if(!inserted)
    {
        throw types::errors::DbWriteError(query.lastError().text().toStdString());
    }
    return query.lastInsertId().toUInt();
}


void WriteableDanceFile::addPosition(types::MoveId moveId, uint32_t beatNum, uint32_t personIdx, const QPointF& pos, uint8_t heading)
{
    QSqlQuery query(this->getQuery());
    query.prepare("INSERT INTO positions(move_id, beat_number, person_idx, pos_x, pos_y, direction) VALUES(:move_id, :beat_number, :person_idx, :pos_x, :pos_y, :heading)");
    query.bindValue(":move_id", static_cast<uint32_t>(moveId));
    query.bindValue(":beat_number", beatNum);
    query.bindValue(":person_idx", personIdx);
    query.bindValue(":pos_x", pos.x());
    query.bindValue(":pos_y", pos.y());
    query.bindValue(":heading", static_cast<uint32_t>(heading));
    bool inserted = query.exec();
    if(!inserted)
    {
        throw types::errors::DbWriteError(query.lastError().text().toStdString());
    }
}


}}
