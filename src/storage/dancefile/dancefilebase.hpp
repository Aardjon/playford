/**
 * Declaration of the DanceFile class.
 */

#ifndef STORAGE_DANCEFILE_DANCEFILE_HPP_
#define STORAGE_DANCEFILE_DANCEFILE_HPP_

#include <QPointF>
#include <QString>
#include <QSqlQuery>

#include "types/danceid.hpp"
#include "types/moveid.hpp"
#include "types/formation/danceformation.hpp"


namespace storage {
namespace dancefile {

class IDanceFileReadResultDelegate {
public:

    virtual void readDanceResultRow(
            uint32_t danceDbId,
            const QString& name,
            const types::formation::GlobalDanceFormationId& formationId,
            const QString& sourceString) = 0;

    virtual void readMoveResultRow(uint32_t moveDbId, const QString& label) = 0;

    virtual void readBeatResultRow(uint32_t moveDbId, uint32_t beatIdx) = 0;

    virtual void readPositionResultRow(
            uint32_t moveDbId,
            uint32_t beatNum,
            uint32_t personIdx,
            const QPointF& pos,
            uint8_t heading) = 0;
};


class DanceFileBase {
public:
    virtual ~DanceFileBase();

private:
	static const uint32_t DATABASE_SCHEMA_VERSION;
	static const char* const DATABASE_SCHEMA_DEFINITION;
	static const char* const DATABASE_CONNECTION_NAME;

protected:
    DanceFileBase(const QString& filename, const QString& dbConnectOptions="");

    QSqlQuery getQuery();

	void createDbSchema();
    bool isSchemaCompatible();
};


} /* namespace dancefile */
} /* namespace storage */

#endif /* STORAGE_DANCEFILE_DANCEFILE_HPP_ */
