#ifndef WRITEABLEDANCEFILE_HPP
#define WRITEABLEDANCEFILE_HPP

#include <QPointF>
#include <QString>

#include "types/danceid.hpp"
#include "types/moveid.hpp"
#include "types/formation/danceformation.hpp"

#include "storage/dancefile/dancefilebase.hpp"


namespace storage {
namespace dancefile {


class WriteableDanceFile : public DanceFileBase
{
public:
    WriteableDanceFile(const QString& filename);

    types::DanceId createDance(
            const QString& name,
            const types::formation::GlobalDanceFormationId& formationId,
            const QString& sourceString);

    types::MoveId addMove(types::DanceId danceId, const QString& moveLabel);

    void addPosition(types::MoveId moveId, uint32_t beatNum, uint32_t personIdx, const QPointF& pos, uint8_t heading);
};

}}

#endif // WRITEABLEDANCEFILE_HPP
