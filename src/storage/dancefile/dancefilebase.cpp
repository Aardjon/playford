/**
 * Implementation of the DanceFile class.
 */

#include <QVariant>
#include <QSqlError>
#include <QDebug>

#include "storage/dancefile/dancefilebase.hpp"
#include "types/errors/dbwriteerror.hpp"
#include "types/errors/fileaccesserror.hpp"

namespace storage {
namespace dancefile {

const uint32_t DanceFileBase::DATABASE_SCHEMA_VERSION = 1;

const char* const DanceFileBase::DATABASE_SCHEMA_DEFINITION =
		"CREATE TABLE IF NOT EXISTS schema_version("
		"	version INTEGER"
        ");"
        ""
        "CREATE TABLE IF NOT EXISTS dances("
        "   id INTEGER PRIMARY KEY,"
        "   name STRING,"
        "   formation STRING,"
        "   source STRING"
        ");"
        ""
        "CREATE TABLE IF NOT EXISTS moves("
        "   id INTEGER PRIMARY KEY,"
        "   dance_id INTEGER,"
        "   label STRING,"
        "   FOREIGN KEY(dance_id) REFERENCES dances(id)"
        ");"
        ""
        "CREATE TABLE IF NOT EXISTS positions("
        "   move_id INTEGER,"
        "   beat_number INTEGER,"
        "   person_idx INTEGER,"
        "   pos_x REAL,"
        "   pos_y REAL,"
        "   direction INTEGER,"
        "   FOREIGN KEY(move_id) REFERENCES moves(id),"
        "   PRIMARY KEY(move_id, beat_number, person_idx)"
        ")";


const char* const DanceFileBase::DATABASE_CONNECTION_NAME = "storage.dancefile.writer";


DanceFileBase::DanceFileBase(const QString& filename, const QString& dbConnectOptions)
{
	QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE", DATABASE_CONNECTION_NAME);
	database.setDatabaseName(filename);
    if("" != dbConnectOptions)
    {
        database.setConnectOptions(dbConnectOptions);
    }
	if(database.open() == false)
	{
        throw types::errors::FileAccessError(database.lastError().text().toStdString());
	}
}


DanceFileBase::~DanceFileBase()
{
	{
		QSqlDatabase database = QSqlDatabase::database(DATABASE_CONNECTION_NAME);
		database.commit();
		database.close();
	}
	QSqlDatabase::removeDatabase(DATABASE_CONNECTION_NAME);
}


QSqlQuery DanceFileBase::getQuery()
{
	return QSqlQuery(QSqlDatabase::database(DATABASE_CONNECTION_NAME));
}




void DanceFileBase::createDbSchema()
{
    QSqlQuery query(this->getQuery());

    QStringList sqlStatements = QString(DATABASE_SCHEMA_DEFINITION).split(";");
    for(auto statementIter=sqlStatements.begin(); statementIter!=sqlStatements.end(); ++statementIter)
    {
        bool success = query.exec(*statementIter);
        if(!success)
        {
            throw types::errors::DbWriteError(query.lastError().text().toStdString());
        }
    }

    query.prepare("INSERT INTO schema_version(version) VALUES (:version)");
    query.bindValue(":version", DATABASE_SCHEMA_VERSION);
    bool success = query.exec();
    if(success == false)
    {
        throw types::errors::DbWriteError(query.lastError().text().toStdString());
    }
}


bool DanceFileBase::isSchemaCompatible()
{
    QSqlQuery query(this->getQuery());
    query.prepare("SELECT version FROM schema_version LIMIT 1");
    bool success = query.exec();
    if(success)
    {
        query.next();
        return DATABASE_SCHEMA_VERSION == query.value(0).toUInt();
    }
    return false;
}


} /* namespace dancefile */
} /* namespace storage */
