#include <QSqlError>
#include <QVariant>

#include "storage/dancefile/readonlydancefile.hpp"
#include "types/errors/invaliddancefileerror.hpp"


namespace storage {
namespace dancefile {


ReadonlyDanceFile::ReadonlyDanceFile(const QString& filename)
    : DanceFileBase(filename, "QSQLITE_OPEN_READONLY")
{
    if(false == this->isSchemaCompatible())
    {
        throw types::errors::InvalidDanceFileError("DB Schema is not compatible.");
    }
}


void ReadonlyDanceFile::loadDance(IDanceFileReadResultDelegate& resultDelegate)
{
    QSqlQuery query(this->getQuery());
    query.prepare("SELECT id, name, formation, source FROM dances LIMIT 1");
    bool success = query.exec();
    if(!success)
    {
        raiseDbError(query);
    }
    if(false == query.next())
    {
        throw types::errors::InvalidDanceFileError("Dance file does not contain any dance data.");
    }
    resultDelegate.readDanceResultRow(
                query.value(0).toUInt(),
                query.value(1).toString(),
                query.value(2).toString().toStdString(),
                query.value(3).toString());
}


void ReadonlyDanceFile::loadMoves(uint32_t danceDbId, IDanceFileReadResultDelegate& resultDelegate)
{
    QSqlQuery query(this->getQuery());
    query.prepare("SELECT id, label FROM moves WHERE dance_id=:danceid");
    query.bindValue(":danceid", danceDbId);
    bool success = query.exec();
    if(!success)
    {
        raiseDbError(query);
    }
    while(query.next())
    {
        resultDelegate.readMoveResultRow(query.value(0).toUInt(), query.value(1).toString());
    }
}



void ReadonlyDanceFile::loadBeats(uint32_t moveDbId, IDanceFileReadResultDelegate& resultDelegate)
{
    QSqlQuery query(this->getQuery());
    query.prepare(
                "SELECT DISTINCT beat_number FROM positions WHERE move_id=:moveid ORDER BY beat_number ASC");
    query.bindValue(":moveid", moveDbId);
    bool success = query.exec();
    if(!success)
    {
        raiseDbError(query);
    }
    while(query.next())
    {
        resultDelegate.readBeatResultRow(moveDbId, query.value(0).toUInt());
    }
}



void ReadonlyDanceFile::loadPositions(uint32_t moveDbId, uint32_t beatIdx, IDanceFileReadResultDelegate& resultDelegate)
{
    QSqlQuery query(this->getQuery());
    query.prepare(
                "SELECT beat_number, person_idx, pos_x, pos_y, direction FROM positions WHERE move_id=:moveid AND beat_number=:beatidx ORDER BY beat_number, person_idx ASC"
                );
    query.bindValue(":moveid", moveDbId);
    query.bindValue(":beatidx", beatIdx);
    bool success = query.exec();
    if(!success)
    {
        raiseDbError(query);
    }
    while(query.next())
    {
        QPointF position(query.value(2).toReal(), query.value(3).toReal());
        uint8_t heading = static_cast<uint8_t>(query.value(4).toUInt());
        resultDelegate.readPositionResultRow(moveDbId, query.value(0).toUInt(), query.value(1).toUInt(), position, heading);
    }
}


void ReadonlyDanceFile::raiseDbError(const QSqlQuery& query) const
{
    throw types::errors::InvalidDanceFileError(query.lastError().text().toStdString());
}

}}
