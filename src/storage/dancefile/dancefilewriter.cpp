/**
 * Implementation of the DanceFileWriter class.
 */

#include <QFile>

#include "storage/dancefile/dancefilewriter.hpp"


namespace storage {
namespace dancefile {

DanceFileWriter::DanceFileWriter(const types::Dance& dance)
        : m_Dance(dance)
{
}

DanceFileWriter::~DanceFileWriter()
{
}


void DanceFileWriter::writeDanceToFile(const QString& filename)
{
    truncateDbFile(filename);
    WriteableDanceFile dbFile(filename);
    writeDance(dbFile);
}

void DanceFileWriter::truncateDbFile(const QString& filename)
{
    if(QFile::exists(filename))
    {
        QFile dbFile(filename);
        dbFile.open(QFile::WriteOnly);
        dbFile.resize(0);
        dbFile.close();
    }
}



void DanceFileWriter::writeDance(WriteableDanceFile& database)
{
    types::DanceId danceId = database.createDance(m_Dance.getName(), m_Dance.getGlobalFormationId(), m_Dance.getSourceString());
    const types::StdVector_MoveId& moveIds = m_Dance.getMoveIds();
    for(auto moveIdIter=moveIds.begin(); moveIdIter!=moveIds.end(); ++moveIdIter)
    {
        writeMove(database, danceId, *moveIdIter);
    }
}


void DanceFileWriter::writeMove(WriteableDanceFile& database, types::DanceId danceId, types::MoveId moveId)
{
    const QString& moveLabel = m_Dance.getMoveLabel(moveId);
    uint32_t dbMoveId = database.addMove(danceId, moveLabel);
    const types::StdVector_BeatId& beatIds = m_Dance.getBeatIds(moveId);
    for(uint32_t beatNum=0; beatNum<beatIds.size(); ++beatNum)
    {
        types::BeatId beatId = beatIds.at(beatNum);
        writeBeat(database, dbMoveId, beatNum, beatId);
    }
}



void DanceFileWriter::writeBeat(WriteableDanceFile& database, types::MoveId moveId, uint32_t beatIdx, types::BeatId beatId)
{
    const types::Beat& beat = m_Dance.getBeat(beatId);
    for(uint32_t personIdx=0; personIdx<m_Dance.getPersonCount(); ++personIdx)
    {
        const QPointF& position = beat.getPersonsPosition(personIdx);
        uint8_t heading = beat.getPersonsHeading(personIdx);
        database.addPosition(moveId, beatIdx, personIdx, position, heading);
    }
}


} /* namespace dancefile */
} /* namespace storage */
