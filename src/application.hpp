#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <memory>
#include <QApplication>
#include <QGraphicsScene>

#include "dancemodel.hpp"
#include "gui/mainwindow.hpp"
#include "types/formation/danceformation.hpp"


class PlayfordApplication : public QObject
{
	Q_OBJECT

public:
    PlayfordApplication(int& argc, char **argv);
    ~PlayfordApplication();
    int exec();

private:
    QApplication m_QtAppObj;
    DanceModel m_DanceModel;
    std::unique_ptr<gui::MainWindow> m_MainWindow;

private slots:
    void actionNewDance(
            const QString& danceName,
            const types::formation::GlobalDanceFormationId& formationId,
            const QString& sourceString);
	void actionSaveDance(const QString& fileName);
    void actionLoadDance(const QString& fileName);
};

#endif // APPLICATION_HPP
