#include <QPainter>
#include <QDebug>
#include "types/itemid.hpp"
#include "gui/beatedit/beateditorscene.hpp"
#include "gui/beatedit/malepersongraphicsitem.hpp"
#include "gui/beatedit/femalepersongraphicsitem.hpp"

namespace gui { namespace beatedit {


BeatEditorScene::BeatEditorScene(DanceModel& danceModel)
    : m_DanceModel(danceModel),
    m_CurrentBeatIndex(QModelIndex())
{
    m_Scene.setSceneRect(-150.0, -150.0, 300.0, 300.0);
    this->onModelReset();
    connect(&m_DanceModel, SIGNAL(modelReset()), this, SLOT(onModelReset()));
    connect(&m_Scene, SIGNAL(changed(QList<QRectF>)), this, SLOT(onSceneChanged(QList<QRectF>)));
}



void BeatEditorScene::onModelReset()
{
    m_CurrentBeatIndex = QModelIndex();
    m_PersonItems.clear();
    m_Scene.clear();

    for(uint32_t personIdx=0; personIdx<m_DanceModel.getPersonCount(); ++personIdx)
    {
        if(personIdx % 2 == 0)
        {
            std::shared_ptr<MalePersonGraphicsItem> maleItem(new MalePersonGraphicsItem(personIdx));
            m_PersonItems.push_back(maleItem);
        }
        else
        {
            std::shared_ptr<FemalePersonGraphicsItem> femaleItem(new FemalePersonGraphicsItem(personIdx));
            m_PersonItems.push_back(femaleItem);
        }
    }


    for(auto item=m_PersonItems.begin(); item!=m_PersonItems.end(); ++item)
    {
        m_Scene.addItem(item->get());
    }

    this->enableAllPersons(false);
}


void BeatEditorScene::onSceneChanged(const QList<QRectF>& region)
{
    Q_UNUSED(region);
    this->writeCurrentBeatToModel();
}


void BeatEditorScene::setCurrentBeat(const QModelIndex& danceModelIndex)
{
    // First, write the current beat back to the model
    this->writeCurrentBeatToModel();

    // Now switch to the new beat data
    if(types::ItemId::LevelType::LEVEL_BEAT != types::ItemId(danceModelIndex).getLevelType())
    {
        this->enableAllPersons(false);
        m_CurrentBeatIndex = QModelIndex();
    }
    else
    {
        this->readCurrentBeatFromModel(danceModelIndex);
        this->enableAllPersons(true);
    }
}


void BeatEditorScene::reset()
{
    this->writeCurrentBeatToModel();
    this->enableAllPersons(false);
    m_CurrentBeatIndex = QModelIndex();
}


void BeatEditorScene::writeCurrentBeatToModel()
{
    if(!m_CurrentBeatIndex.isValid())
    {
        return;
    }

    types::Beat beat = m_DanceModel.getBeat(m_CurrentBeatIndex);
    for(uint32_t i=0; i<m_PersonItems.size(); ++i)
    {
        const auto& personItem = *m_PersonItems[i];
        beat.setPersonsPosition(i, personItem.pos());
        beat.setPersonsHeading(i, personItem.getHeading());
    }
    m_DanceModel.setBeat(m_CurrentBeatIndex, beat);
}


void BeatEditorScene::readCurrentBeatFromModel(const QModelIndex& danceModelIndex)
{
    const types::Beat& beat = m_DanceModel.getBeat(danceModelIndex);
    for(uint32_t i=0; i<m_PersonItems.size(); ++i)
    {
        m_PersonItems[i]->setPos(beat.getPersonsPosition(i));
        m_PersonItems[i]->setHeading(beat.getPersonsHeading(i));
    }
    m_CurrentBeatIndex = danceModelIndex;
}


void BeatEditorScene::enableAllPersons(bool enable)
{
    for(auto item=m_PersonItems.begin(); item!=m_PersonItems.end(); ++item)
    {
        (*item)->setEnabled(enable);
        (*item)->setVisible(enable);
    }
}


}}
