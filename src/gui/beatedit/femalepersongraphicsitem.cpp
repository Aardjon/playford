#include <QPainter>

#include "gui/beatedit/femalepersongraphicsitem.hpp"

namespace gui { namespace beatedit {


FemalePersonGraphicsItem::FemalePersonGraphicsItem(uint32_t personIndex)
    : PersonGraphicsItem(personIndex)
{
}


void FemalePersonGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(QPen(Qt::red, 2, Qt::SolidLine));
    QRectF labelRect = boundingRect();
    labelRect.setHeight(labelRect.height()-2);
    painter->drawText(labelRect, Qt::AlignCenter, this->getLabel());
    /*
     * Wie funktioniert drawChord()?
     * rect: Das Rechteck (hier: Quadrat) welches den Kreis (Inkreis) umschließt
     * startAngle: Winkel auf dem Kreisbogen an dem mit Zeichnen begonnen wird (0 = "Osten")
     * endAngle: Kreisbogenwinkel der von startAngle aus gezeichnet werden soll (gegen den Uhrzeigersinn)
    */
    painter->drawChord(QRectF(-15.0, -15.0, 30.0, 30.0), 16*180, 16*180);
}


}}
