#include "personitemtoolbar.hpp"
#include "ui_personitemtoolbar.h"

namespace gui { namespace beatedit {

PersonItemToolbar::PersonItemToolbar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PersonItemToolbar),
    m_RotateLeftCallback([](){}),
    m_RotateRightCallback([](){})
{
    ui->setupUi(this);
}

PersonItemToolbar::~PersonItemToolbar()
{
    delete ui;
}


void PersonItemToolbar::setRotateLeftBtnCallback(ButtonClickedCallback callback)
{
    m_RotateLeftCallback = callback;
}


void PersonItemToolbar::setRotateRightBtnCallback(ButtonClickedCallback callback)
{
    m_RotateRightCallback = callback;
}


void PersonItemToolbar::on_rotateLeftBtn_clicked()
{
    m_RotateLeftCallback();
}


void PersonItemToolbar::on_rotateRightBtn_clicked()
{
    m_RotateRightCallback();
}


}}




