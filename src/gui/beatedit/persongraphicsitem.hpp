#ifndef GUI_BEATEDIT_PERSONGRAPHICSITEM_HPP
#define GUI_BEATEDIT_PERSONGRAPHICSITEM_HPP

#include <memory>
#include <QGraphicsProxyWidget>

#include "gui/beatedit/personitemtoolbar.hpp"


namespace gui { namespace beatedit {


class PersonGraphicsItem : public QGraphicsItem
{
protected:
    uint32_t m_PersonIndex;

    QString getLabel() const;


public:
    PersonGraphicsItem(uint32_t personIndex);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) = 0;
    virtual QRectF boundingRect() const override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent*) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent*) override;

    inline
    uint8_t getHeading() const { return m_Heading; }

    void setHeading(uint8_t heading);


private:
    std::shared_ptr<QGraphicsProxyWidget> m_PersonItemToolbar;

    /** Heading ("viewing direction") of this dancer.
     * This is a value from 0 to 7, 0 being "north". Each increment rotates the person by 45° clockwise.
     */
    uint8_t m_Heading;

    void onRotateLeftClicked();
    void onRotateRightClicked();
    void rotate();

    QPointF calculateToolbarPosition() const;
};

}}

#endif // GUI_BEATEDIT_PERSONGRAPHICSITEM_HPP
