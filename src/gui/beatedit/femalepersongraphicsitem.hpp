#ifndef FEMALEPERSONGRAPHICSITEM_HPP
#define FEMALEPERSONGRAPHICSITEM_HPP

#include "gui/beatedit/persongraphicsitem.hpp"


namespace gui { namespace beatedit {


class FemalePersonGraphicsItem : public PersonGraphicsItem
{
public:
    FemalePersonGraphicsItem(uint32_t personIndex);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;
};


}}

#endif // FEMALEPERSONGRAPHICSITEM_HPP
