#include <QGraphicsScene>
#include <QDebug>
#include <QPushButton>
#include <QIcon>

#include "persongraphicsitem.hpp"
#include "personitemtoolbar.hpp"


namespace gui { namespace beatedit {

PersonGraphicsItem::PersonGraphicsItem(uint32_t personIndex)
    : QGraphicsItem(),
      m_PersonIndex(personIndex),
      m_PersonItemToolbar(new QGraphicsProxyWidget(this)),
      m_Heading(0)
{
    this->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
    this->setTransformOriginPoint(0.0, 7.5);

    PersonItemToolbar* toolbarWidget = new PersonItemToolbar();
    m_PersonItemToolbar->setFlag(GraphicsItemFlag::ItemIgnoresTransformations, true);
    m_PersonItemToolbar->setWidget(toolbarWidget);
    m_PersonItemToolbar->setVisible(false);
    m_PersonItemToolbar->setPos(this->calculateToolbarPosition());

    toolbarWidget->setRotateLeftBtnCallback(std::bind(&PersonGraphicsItem::onRotateLeftClicked, this));
    toolbarWidget->setRotateRightBtnCallback(std::bind(&PersonGraphicsItem::onRotateRightClicked, this));
    this->setAcceptHoverEvents(true);
}


QPointF PersonGraphicsItem::calculateToolbarPosition() const
{
    QPointF itemCenterScenePos = this->mapToScene(0.0, boundingRect().y());
    QPointF toolbarScenePos(itemCenterScenePos.x(), itemCenterScenePos.y() - 15);
    return this->mapFromScene(toolbarScenePos);
}


QRectF PersonGraphicsItem::boundingRect() const
{
    return QRectF(-15.0, 0.0, 30.0, 15.0);
}


void PersonGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent*)
{
    m_PersonItemToolbar->setVisible(true);
}

void PersonGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent*)
{
    m_PersonItemToolbar->setVisible(false);
}


QString PersonGraphicsItem::getLabel() const
{
    return QString::number((m_PersonIndex / 2) + 1);
}


void PersonGraphicsItem::onRotateLeftClicked()
{
    if(0 == m_Heading)
    {
        m_Heading = 8;
    }
    m_Heading -= 1;
    this->rotate();
}


void PersonGraphicsItem::onRotateRightClicked()
{
    m_Heading += 1;
    if(m_Heading == 8) {
        m_Heading = 0;
    }
    this->rotate();
}


void PersonGraphicsItem::setHeading(uint8_t heading)
{
    if(heading > 7)
    {
        throw std::exception();
    }
    m_Heading = heading;
    this->rotate();
}

void PersonGraphicsItem::rotate()
{
    qreal rotationAngle = m_Heading * 45.0;
    // Rotating the item moves the toolbar to another position (since the anchor point is moved).
    // To prevent this, we store the original scene(!) coordinates (which do not change), calculate
    // the new local coordinates of this same point after the rotation and move the toolbar back to
    // its original position.
    QPointF toolbarScenePos = this->m_PersonItemToolbar->scenePos();
    this->setRotation(rotationAngle);
    QPointF toolbarLocalPos = this->mapFromScene(toolbarScenePos);
    this->m_PersonItemToolbar->setPos(toolbarLocalPos);
}

}}
