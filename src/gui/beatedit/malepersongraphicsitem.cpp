#include <iostream>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "gui/beatedit/malepersongraphicsitem.hpp"


namespace gui { namespace beatedit {


MalePersonGraphicsItem::MalePersonGraphicsItem(uint32_t personIndex)
    : PersonGraphicsItem(personIndex)
{
    m_Triangle.append(QPointF(-15.0, 0.0));
    m_Triangle.append(QPointF(15.0, 0.0));
    m_Triangle.append(QPointF(0.0, 15.0));
    m_Triangle.append(QPointF(-15.0, 0.0));
}



void MalePersonGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(QPen(Qt::black, 2, Qt::SolidLine));
    painter->drawPolygon(this->m_Triangle);
    QRectF labelRect = boundingRect();
    labelRect.setHeight(labelRect.height()-3);
    painter->drawText(labelRect, Qt::AlignCenter, this->getLabel());
}


}}
