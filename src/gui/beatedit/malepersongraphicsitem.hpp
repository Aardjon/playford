#ifndef MALEPERSONGRAPHICSITEM_HPP
#define MALEPERSONGRAPHICSITEM_HPP

#include "gui/beatedit/persongraphicsitem.hpp"


namespace gui { namespace beatedit {


class MalePersonGraphicsItem : public PersonGraphicsItem
{
private:
    QPolygonF m_Triangle;

public:
    MalePersonGraphicsItem(uint32_t personIndex);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;
};


}}

#endif // MALEPERSONGRAPHICSITEM_HPP
