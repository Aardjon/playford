#ifndef GUI_BEATEDIT_PERSONITEMTOOLBAR_HPP
#define GUI_BEATEDIT_PERSONITEMTOOLBAR_HPP

#include <functional>
#include <QWidget>

namespace Ui {
class PersonItemToolbar;
}

namespace gui { namespace beatedit {


typedef std::function<void()> ButtonClickedCallback;

class PersonItemToolbar : public QWidget
{
    Q_OBJECT

public:
    explicit PersonItemToolbar(QWidget *parent = nullptr);

    ~PersonItemToolbar();

    void setRotateLeftBtnCallback(ButtonClickedCallback callback);
    void setRotateRightBtnCallback(ButtonClickedCallback callback);

private slots:
    void on_rotateRightBtn_clicked();
    void on_rotateLeftBtn_clicked();

private:
    Ui::PersonItemToolbar *ui;

    ButtonClickedCallback m_RotateLeftCallback;
    ButtonClickedCallback m_RotateRightCallback;
};

}}

#endif // GUI_BEATEDIT_PERSONITEMTOOLBAR_HPP
