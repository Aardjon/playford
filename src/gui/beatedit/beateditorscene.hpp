#ifndef GUI_MOVESCENE_HPP
#define GUI_MOVESCENE_HPP

#include <memory>

#include <QGraphicsScene>
#include <QObject>


#include "gui/beatedit/persongraphicsitem.hpp"
#include "dancemodel.hpp"

namespace gui { namespace beatedit {


class BeatEditorScene : public QObject
{
    Q_OBJECT

private:
    DanceModel& m_DanceModel;
    QGraphicsScene m_Scene;
    std::vector<std::shared_ptr<gui::beatedit::PersonGraphicsItem>> m_PersonItems;
    QModelIndex m_CurrentBeatIndex;

    void writeCurrentBeatToModel();
    void readCurrentBeatFromModel(const QModelIndex& danceModelIndex);
    void enableAllPersons(bool enable);

private slots:
    void onModelReset();
    void onSceneChanged(const QList<QRectF> &region);

public:
    BeatEditorScene(DanceModel& danceModel);

    inline QGraphicsScene& getScene() { return m_Scene; }
    void setCurrentBeat(const QModelIndex& danceModelIndex);
    void reset();
};


}}

#endif // GUI_MOVESCENE_HPP
