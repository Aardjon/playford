#include <QPushButton>
#include <QLineEdit>

#include "newdancedialog.hpp"
#include "ui_newdancedialog.h"

#include "types/formation/danceformationfactory.hpp"


namespace gui {


NewDanceDialog::NewDanceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewDanceDialog)
{
    using namespace types::formation;

    ui->setupUi(this);
    this->setFixedSize(this->size());
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    const StdVector_DanceFormationPtr& formations = DanceFormationFactory::getAll();
    for(uint32_t i=0; i<formations.size(); ++i)
    {
        ui->cbFormation->insertItem(static_cast<int>(i), formations.at(i)->getLabel(), QString::fromStdString(formations.at(i)->getGlobalId()));
    }
}


NewDanceDialog::~NewDanceDialog()
{
    delete ui;
}



QString NewDanceDialog::getDanceName() const
{
    return ui->txtDanceName->text();
}


types::formation::GlobalDanceFormationId NewDanceDialog::getFormationId() const
{
    return ui->cbFormation->currentData().toString().toStdString();
}



QString NewDanceDialog::getSourceString() const
{
    return ui->txtSource->text();
}


void NewDanceDialog::on_txtDanceName_textChanged(const QString &newText)
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!newText.isEmpty());
}


}
