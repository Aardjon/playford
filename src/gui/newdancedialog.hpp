#ifndef NEWDANCEDIALOG_HPP
#define NEWDANCEDIALOG_HPP

#include <QDialog>

#include "types/formation/danceformation.hpp"


namespace Ui {
class NewDanceDialog;
}

namespace gui {


class NewDanceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewDanceDialog(QWidget *parent = nullptr);
    ~NewDanceDialog();

    QString getDanceName() const;

    types::formation::GlobalDanceFormationId getFormationId() const;

    QString getSourceString() const;

private slots:
    void on_txtDanceName_textChanged(const QString &newText);

private:
    Ui::NewDanceDialog *ui;
};

}

#endif // NEWDANCEDIALOG_HPP
