#ifndef GUI_MAINWINDOW_HPP
#define GUI_MAINWINDOW_HPP

#include <QMainWindow>


#include "dancemodel.hpp"
#include "gui/beatedit/beateditorscene.hpp"
#include "types/formation/danceformation.hpp"

namespace Ui {
class MainWindow;
}

namespace gui {

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(DanceModel& danceModel);
    ~MainWindow();

signals:
    void newDance(
            const QString& danceName,
            const types::formation::GlobalDanceFormationId& formationId,
            const QString& sourceString);
	void saveDance(const QString& filename);
    void loadDance(const QString& filename);

private:
    Ui::MainWindow *ui;
    DanceModel& m_DanceModel;
    gui::beatedit::BeatEditorScene m_BeatEditScene;

private slots:
    void onCurrentBeatChanged(const QModelIndex &current, const QModelIndex &previous);

    void on_loadDanceBtn_triggered();
    void on_saveDanceBtn_triggered();
    void on_newDanceBtn_triggered();
    void on_addMoveBtn_triggered();
    void on_addBeatBtn_triggered();
    void on_removeBtn_triggered();
};

}

#endif // GUI_MAINWINDOW_HPP
