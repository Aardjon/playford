/**
 * Implementation of the MainWindows class.
 */

#include <iostream>
#include <QFileDialog>
#include <QStandardPaths>

#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include "version.hpp"
#include "gui/newdancedialog.hpp"
#include "types/itemid.hpp"


namespace gui {

MainWindow::MainWindow(DanceModel& danceModel) :
    QMainWindow(nullptr),
    ui(new Ui::MainWindow),
    m_DanceModel(danceModel),
    m_BeatEditScene(m_DanceModel)
{
    ui->setupUi(this);
    this->setWindowTitle(this->windowTitle() + " (Version " + APPVERSIONSTRING + ")");
    ui->danceView->setModel(&m_DanceModel);
    ui->beatEditor->setScene(&m_BeatEditScene.getScene());

    connect(ui->danceView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(onCurrentBeatChanged(const QModelIndex&, const QModelIndex&)));
}



MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onCurrentBeatChanged(const QModelIndex &current, const QModelIndex &previous)
{
    Q_UNUSED(previous);


    if(current.isValid())
    {
        ui->removeBtn->setEnabled(true);
        ui->addBeatBtn->setEnabled(true);
    }
    else
    {
        ui->removeBtn->setEnabled(false);
        ui->addBeatBtn->setEnabled(false);
    }
    m_BeatEditScene.setCurrentBeat(current);
}


void MainWindow::on_addBeatBtn_triggered()
{
    // Create a new Beat within the current Move
    // "Add beat" button must not be enabled when no move or beat is selected!

    // Find the currently selected Move
    QModelIndex moveIdx;
    QModelIndex selectedModelIdx = ui->danceView->selectionModel()->currentIndex();
    if(types::ItemId(selectedModelIdx).getLevelType() == types::ItemId::LEVEL_BEAT)
    {
        moveIdx = selectedModelIdx.parent();
    }
    else
    {
        moveIdx = selectedModelIdx;
    }
    m_DanceModel.insertRow(m_DanceModel.rowCount(moveIdx), moveIdx);
}


void MainWindow::on_addMoveBtn_triggered()
{
    m_DanceModel.insertRow(m_DanceModel.rowCount(QModelIndex()), QModelIndex());
}


void MainWindow::on_removeBtn_triggered()
{
    m_BeatEditScene.reset();
    QModelIndex selectedModelIdx = ui->danceView->selectionModel()->currentIndex();
    m_DanceModel.removeRow(selectedModelIdx.row(), selectedModelIdx.parent());
}


void MainWindow::on_newDanceBtn_triggered()
{
    NewDanceDialog dlg(this);
    if(QDialog::Accepted == dlg.exec())
    {
        emit newDance(dlg.getDanceName(), dlg.getFormationId(), dlg.getSourceString());
    }
}


void MainWindow::on_saveDanceBtn_triggered()
{
	const QString& documentDirectoryPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
	const QString& fileName = QFileDialog::getSaveFileName(this, "Tanz speichern", documentDirectoryPath, "Playford-Dateien (*.pf)");
    if(!fileName.isEmpty())
    {
        emit saveDance(fileName);
    }
}


void MainWindow::on_loadDanceBtn_triggered()
{
    const QString& documentDirectoryPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    const QString& fileName = QFileDialog::getOpenFileName(this, "Tanz laden", documentDirectoryPath, "Playford-Dateien (*.pf)");
    if(!fileName.isEmpty())
    {
        emit loadDance(fileName);
    }
}

}
