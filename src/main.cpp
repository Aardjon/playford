#include "application.hpp"

int main(int argc, char *argv[])
{
    PlayfordApplication app(argc, argv);
    return app.exec();
}
