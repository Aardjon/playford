#include <stdexcept>

#include <QMessageBox>

#include "application.hpp"
#include "storage/dancefile/dancefilewriter.hpp"
#include "storage/dancefile/dancefilereader.hpp"
#include "types/formation/danceformationfactory.hpp"


PlayfordApplication::PlayfordApplication(int& argc, char **argv)
    :
      m_QtAppObj(argc, argv),
      m_DanceModel(types::Dance::createDefaultDance(
                       "Neuer Tanz",
                       types::formation::DanceFormationFactory::getAll().at(0),
                       "[unbekannt]")),
      m_MainWindow(new gui::MainWindow(m_DanceModel))
{
	connect(m_MainWindow.get(), SIGNAL(saveDance(const QString&)), this, SLOT(actionSaveDance(const QString&)));
    connect(m_MainWindow.get(), SIGNAL(loadDance(const QString&)), this, SLOT(actionLoadDance(const QString&)));
    connect(
                m_MainWindow.get(),
                SIGNAL(newDance(const QString&, const types::formation::GlobalDanceFormationId&, const QString&)),
                this,
                SLOT(actionNewDance(const QString&,  const types::formation::GlobalDanceFormationId&, const QString&))
           );

    if(m_QtAppObj.arguments().size() > 1)
    {
        this->actionLoadDance(m_QtAppObj.arguments().at(1));
    }
}


PlayfordApplication::~PlayfordApplication()
{
    types::formation::DanceFormationFactory::cleanupKnownFormations();
}


int PlayfordApplication::exec()
{
    m_MainWindow->show();
    return m_QtAppObj.exec();
}


void PlayfordApplication::actionNewDance(
        const QString& danceName,
        const types::formation::GlobalDanceFormationId& formationId,
        const QString& sourceString)
{
    types::Dance newDance = types::Dance::createDefaultDance(
                danceName,
                types::formation::DanceFormationFactory::get(formationId),
                sourceString);
    this->m_DanceModel.setDance(newDance);
}


void PlayfordApplication::actionSaveDance(const QString& filename)
{
	try
	{
		storage::dancefile::DanceFileWriter danceWriter(this->m_DanceModel.getDance());
		danceWriter.writeDanceToFile(filename);
	}
    catch(const std::runtime_error& ex)
	{
        QString message("Die Datei '%1' kann nicht geschrieben werden.\n\nFehlermeldung: %2");
        QMessageBox::warning(this->m_MainWindow.get(), this->m_QtAppObj.applicationDisplayName(), message.arg(filename, ex.what()));
	}
}


void PlayfordApplication::actionLoadDance(const QString& filename)
{
    try
    {
        storage::dancefile::DanceFileReader danceReader(filename);
        types::Dance loadedDance = danceReader.readDanceFromFile();
        this->m_DanceModel.setDance(loadedDance);
    }
    catch(const std::runtime_error& ex)
    {
        QString message("Die Datei '%1' scheint keine gültigen Tanzdaten zu enthalten und kann deshalb leider nicht geöffnet werden.\n\nFehlermeldung: %2");
        QMessageBox::warning(this->m_MainWindow.get(), this->m_QtAppObj.applicationDisplayName(), message.arg(filename, ex.what()));
    }
}

