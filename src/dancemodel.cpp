#include "types/itemid.hpp"
#include "types/formation/danceformationfactory.hpp"
#include "dancemodel.hpp"

DanceModel::DanceModel(const types::Dance& dance)
    :
      QAbstractItemModel(),
      m_Dance(dance)
{
}



Qt::ItemFlags DanceModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
            return 0;

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if(types::ItemId(index).isLevelMove())
    {
        flags |= Qt::ItemIsEditable;
    }
    return flags;
}



QVariant DanceModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    types::ItemId id(index);
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(id.getLevelType())
            {
                case types::ItemId::LEVEL_MOVE:
                {
                    const QString& move = m_Dance.getMoveLabel(id.getItemId());
                    uint32_t beatCount = m_Dance.getBeatCount(id.getItemId());
                    return move + " (" + QString::number(beatCount) + " Schläge)";
                }
                case types::ItemId::LEVEL_BEAT:
                    return QString("Schlag ") + QString::number(index.row() + 1);
                default:
                    return QVariant();
            }
            break;
        }
        case Qt::EditRole:
        {
            if(id.isLevelMove())
            {
                return m_Dance.getMoveLabel(id.getItemId());
            }
            break;
        }
        default:
            return QVariant();
    }

    return QVariant();
}


QVariant DanceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);

    if (orientation == Qt::Horizontal)
    {
        switch(role)
        {
            case Qt::DisplayRole:
                return m_Dance.getName();
            case Qt::ToolTipRole:
            {
                return m_Dance.getName() + "<br>" + m_Dance.getFormationLabel() + "<br>Quelle: " + m_Dance.getSourceString();
            }
            default:
                break;
        }
    }

    return QVariant();
}



/**
 * @note: Qt View sometimes try to get indices for non-existent row, e.g. after removing the current
 *        selection (probably to select the next item). So index validation is important here!
 * @brief DanceModel::index
 * @param row
 * @param column
 * @param parent
 * @return
 */
QModelIndex DanceModel::index(int row, int column, const QModelIndex &parent) const
{
    if(row < 0 || column < 0)
    {
        return QModelIndex();
    }

    types::StdVector_BeatId::size_type row_idx = static_cast<types::StdVector_BeatId::size_type>(row);
    types::ItemId indexItemId;
    if(!parent.isValid())
    {
        // Oberste Ebene: Figur
        types::StdVector_MoveId moveIds = m_Dance.getMoveIds();
        if(row_idx >= moveIds.size())
        {
            return QModelIndex();
        }
        indexItemId = types::ItemId(types::ItemId::LEVEL_MOVE, moveIds.at(row_idx));
    }
    else
    {
        types::ItemId moveItemId(parent);
        const types::StdVector_BeatId& beatIds = m_Dance.getBeatIds(moveItemId.getItemId());
        if(row_idx >= beatIds.size())
        {
            return QModelIndex();
        }
        types::BeatId beatId = beatIds.at(row_idx);
        indexItemId = types::ItemId(types::ItemId::LEVEL_BEAT, beatId);
    }

    return createIndex(row, column, indexItemId.toModelIndexId());
}



int DanceModel::rowCount(const QModelIndex &parent) const
{
    uint16_t rowCount = 0;

    if(!parent.isValid())
    {
        // Parent-Node ist der Root-Knoten
        rowCount = m_Dance.getMoveCount();
    }
    else
    {
        types::ItemId id(parent);
        switch(id.getLevelType())
        {
            case types::ItemId::LEVEL_MOVE:
            {
                rowCount = m_Dance.getBeatCount(id.getItemId());
                break;
            }
            case types::ItemId::LEVEL_BEAT:
            {
                rowCount = 0;
                break;
            }
            default:
            {
                rowCount = 0;
                break;
            }
        }
    }
    return rowCount;
}



QModelIndex DanceModel::parent(const QModelIndex &index) const
{
    QModelIndex parentModelIndex;
    types::ItemId id(index);
    if(id.isLevelBeat())
    {
        types::MoveId moveId = m_Dance.getMoveOfBeat(id.getItemId());
        types::ItemId parentId(types::ItemId::LEVEL_MOVE, moveId);
        // Find the correct model index (=row) for this move ID
        const types::StdVector_MoveId& moveIds = m_Dance.getMoveIds();
        auto findIter = std::find(moveIds.begin(), moveIds.end(), moveId);
        if(findIter != moveIds.end())
        {
            int moveIdx = findIter - moveIds.begin();
            parentModelIndex = createIndex(moveIdx, index.column(), parentId.toModelIndexId());
        }
    }
    return parentModelIndex;
}



int DanceModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}



const types::Beat& DanceModel::getBeat(const QModelIndex& index) const
{
    return m_Dance.getBeat(types::ItemId(index).getItemId());
}


void DanceModel::setBeat(const QModelIndex& index, const types::Beat& beat)
{
    m_Dance.setBeat(types::ItemId(index).getItemId(), beat);
}



bool DanceModel::insertRows(int row, int count, const QModelIndex &parent)
{
    assert(count == 1);

    types::ItemId parentItemId(parent);
    this->beginInsertRows(parent, row, row);
    if(parent.isValid())
    {
        // Insert a new Beat
        m_Dance.addBeat(parentItemId.getItemId());
    }
    else
    {
        // Insert a new Move
        m_Dance.addMove("Neue Figur");
    }
    this->endInsertRows();
    return true;
}



bool DanceModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if(row < 0 || count < 0)
    {
        return false;
    }

    auto start_row_idx = static_cast<types::StdVector_BeatId::size_type>(row);
    auto end_row_idx = static_cast<types::StdVector_BeatId::size_type>(row+count);

    this->beginRemoveRows(parent, row, row+count-1);
    if(parent.isValid())
    {
        // Remove the selected Beats
        types::ItemId parentItemId(parent);

        const types::StdVector_BeatId& beatIds = m_Dance.getBeatIds(parentItemId.getItemId());
        types::StdVector_BeatId beatIdsToRemove;
        for(auto i=start_row_idx; i<end_row_idx; ++i)
        {
            types::BeatId beatId = beatIds.at(i);
            beatIdsToRemove.push_back(beatId);
        }
        for(auto iter=beatIdsToRemove.begin(); iter!=beatIdsToRemove.end(); ++iter)
        {
            m_Dance.removeBeat(*iter);
        }

    }
    else
    {
        // Remove the selected Move, including all assigned Beats
        QModelIndex moveIdx = this->index(row, 0, parent);
        m_Dance.removeMove(types::ItemId(moveIdx).getItemId());
    }
    this->endRemoveRows();
    return true;
}



bool DanceModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role != Qt::EditRole)
    {
        return false;
    }

    types::ItemId id(index);
    if(!index.isValid() || !id.isLevelMove())
    {
        return false;
    }

    const QString newValue = value.toString();
    if(newValue.isEmpty() || newValue == m_Dance.getMoveLabel(id.getItemId()))
    {
        return false;
    }

    m_Dance.setMoveLabel(id.getItemId(), newValue);
    emit dataChanged(index, index);
    return true;
}



void DanceModel::setDance(const types::Dance& dance)
{
    this->beginResetModel();
    this->m_Dance = dance;
    this->endResetModel();
}
